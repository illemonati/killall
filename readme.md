# killall

simple program to kill a process

## latest builds

[builds](https://gitlab.com/illemonati/killall/-/jobs/artifacts/master/browse/build/?job=build)

## usage

```golang
usage: killall [<flags>] <command> [<args> ...]

Flags:
      --help     Show context-sensitive help (also try --help-long and --help-man).
  -v, --verbose  verbosity
  -t, --track    countinously track and kill processes

Commands:
  help [<command>...]
    Show help.

  list <name>
    list processes

  kill <name>
    kill processes
```
