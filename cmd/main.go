package main

import (
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"github.com/mitchellh/go-ps"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	list     = kingpin.Command("list", "list processes")
	listname = list.Arg("name", `name of process to list (doesn't have to be full name, just contains this)`).Required().String()

	kill     = kingpin.Command("kill", "kill processes")
	killname = kill.Arg("name", `name of process to list (doesn't have to be full name, just contains this)`).Required().String()
	verbose  = kingpin.Flag("verbose", "verbosity").Short('v').Bool()
	track    = kingpin.Flag("track", "countinously track and kill processes").Short('t').Bool()
)

func main() {
	switch kingpin.Parse() {
	case "list":
		fmt.Printf("list processes with names containing [ %s ]\n", *listname)
		listProcesses(*listname)
	case "kill":
		fmt.Printf("killing process with names containing [ %s ]\n", *killname)
		killProcesses(*killname, *verbose, *track)
	}

}

func findProcesses(name string) chan ps.Process {
	c := make(chan ps.Process)
	processes, err := ps.Processes()
	if err != nil {
		close(c)
		return c
	}
	go func() {
		defer close(c)
		for _, process := range processes {
			if strings.Contains(strings.ToLower(process.Executable()), strings.ToLower(name)) {
				c <- process
			}
		}
	}()
	return c
}

func listProcesses(name string) {
	for process := range findProcesses(name) {
		fmt.Printf("%s, %d, %d\n", process.Executable(), process.Pid(), process.PPid())
	}
}

func killProcesses(name string, verbose, track bool) {
	if track {
		for {
			go killProcessesInner(name, verbose)
			time.Sleep(1 * time.Second)
		}
	}
}

func killProcessesInner(name string, verbose bool) {
	for process := range findProcesses(name) {
		osp, err := os.FindProcess(process.Pid())
		if err != nil {
			continue
		}
		osp.Kill()
		if verbose {
			log.Printf("%s, %d, %d killed\n", process.Executable(), process.Pid(), process.PPid())
		}
	}
}
