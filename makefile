GO := go
BINNAME := killall
BUILD_DIR := ./build
SRC_DIR := ./cmd

PLATFORMS=darwin linux windows
ARCHITECTURES=386 amd64 arm arm64

ifeq ($(OS),Windows_NT)
    setenv := set
else
    setenv := export
endif



default: all

all: build-all


build:
	$(GO) build -ldflags="-s -w" -o ${BUILD_DIR}/$(BINNAME) ${SRC_DIR}

build-all:
	$(foreach GOOS, $(PLATFORMS),\
		$(foreach GOARCH, $(ARCHITECTURES), $(shell $(setenv) GOOS=$(GOOS); $(setenv) GOARCH=$(GOARCH); ${GO} build --trimpath -v -ldflags "-s -w" -o ${BUILD_DIR}/$(GOOS)/$(GOOS)-$(GOARCH)/${BINNAME}-$(GOOS)-$(GOARCH)$(if $(filter windows , $(GOOS)),.exe) $(SRC_DIR))))

clean: 
	rm -rf ${BUILD_DIR}

